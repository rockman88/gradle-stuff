# Gradle Stuff
Place to store gradle notes

## gradle.properties
<https://docs.gradle.org/current/userguide/build_environment.html#sec:gradle_configuration_properties>

## Local cache and storage
It default to $USER_HOME/.gradle

To change it do one of the following

-  Create a system variable `GRADLE_USER_HOME`
-  No other way.  Wow, GJ gradle.

# TODO
- Find out how to change cache directory